default: style html ;

style: dist/styles.css ;
dist/styles.css: src/input.css
	npx tailwindcss -m -i $< -o $@

html: dist/index.html ;
dist/index.html: src/index.pug
	npx pug-cli < $< > $@

.PHONY: style html
