# worst todo

this is the worst todo app

## motivation

i wanted to try to build something
using as few 
best practices
and web standards
as possible.
you know.
for fun.

<dl>
<dt>*style: tailwind*</dt>
<dd>write inline utility classes instead of css</dd>
<dd>https://tailwindcss.com/</dd>

<dt>*markup: pug*</dt>
<dd>write whitespace-significant markup instead of html</dd>
<dd>https://pugjs.org/api/getting-started.html</dd>

<dt>*reactivity: alpine*</dt>
<dd>a minimal, inline javascript framework</dd>
<dd>https://alpinejs.dev/</dd>
</dl>

## dependencies

- material icons (cdn)
- alpine.js (cdn)
- pug (npm)
- tailwind (npm)

you shouldn't need 
to install anything.
icons and alpine are served from cdn.
and pug and tailwind are run with npx.

## getting started

`make`

## notes

this is probably not idiomatic alpine.
i ended up writing a fair bit of "regular" javascript.
